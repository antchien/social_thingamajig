SocialThingamajig::Application.routes.draw do
  resources :users do
    collection do
      get :reset_token
    end
    resources :posts

  end
  resource :session
  resources :friend_circles



end
