class Posts < ActiveRecord::Migration
  def change
    create_table :posts do |t|
      t.text :body, null: false
    end
   end
end
