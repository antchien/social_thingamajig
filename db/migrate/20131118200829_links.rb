class Links < ActiveRecord::Migration
  def change
    create_table :links do |t|
      t.string :link, null: false
      t.integer :post_id, null: false
    end
  end
end
