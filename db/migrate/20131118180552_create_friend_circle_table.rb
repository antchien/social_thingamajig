class CreateFriendCircleTable < ActiveRecord::Migration
  def change
    create_table :friend_circles do |t|
      t.string :name, null: false
    end

    add_index :friend_circles, :name

    create_table :friend_circle_memberships do |t|
      t.integer :user_id, null: false
      t.integer :friend_circle_id, null: false
    end

    add_index :friend_circle_memberships, :friend_circle_id
  end
end
