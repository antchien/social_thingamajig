# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)


User.create(email: 'anthony@appacademy.io', password: 'abc' )
User.create(email: 'sam@appacademy.io', password: '123' )
User.create(email: 'john@appacademy.io', password: 'xyz' )
User.create(email: 'mike@appacademy.io', password: 'qwerty' )
User.create(email: 'tony@appacademy.io', password: 'pass' )
User.create(email: 'tim@appacademy.io', password: 'nopass' )
User.create(email: 'adam@appacademy.io', password: 'hello' )
User.create(email: 'ryan@appacademy.io', password: 'hithere' )
User.create(email: 'dan@appacademy.io', password: 'whatsup' )
User.create(email: 'emil@appacademy.io', password: 'tobeornottobe' )
User.create(email: 'prashant@appacademy.io', password: 'rails' )
User.create(email: 'dale@appacademy.io', password: 'ruby' )
User.create(email: 'granger@appacademy.io', password: 'sql' )
