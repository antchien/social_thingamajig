module SessionsHelper

  def current_user
    User.find_by_session_token(session[:session_token])
  end

  def logged_in?
    return false unless current_user
    true
  end

  def log_in(user)
    user.reset_session_token
    session[:session_token]= user.session_token
  end

  def log_out
    user = current_user
    user.session_token = nil
    user.save!
    session[:session_token] = nil
  end

end
