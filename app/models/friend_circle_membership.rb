class FriendCircleMembership < ActiveRecord::Base
  attr_accessible :user_id, :friend_circle_id

  belongs_to(
  :friend_circle,
  class_name: "FriendCircle",
  foreign_key: :friend_circle_id,
  primary_key: :id
  )

  belongs_to(
  :member,
  class_name: "User",
  foreign_key: :user_id,
  primary_key: :id
  )

end