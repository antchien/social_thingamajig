class Post < ActiveRecord::Base
  attr_accessible :body, :user_id

  belongs_to(
  :author,
  class_name: "User",
  foreign_key: :user_id,
  primary_key: :id
  )

  has_many(
  :links,
  class_name: "Link",
  foreign_key: :post_id,
  primary_key: :id
  )

  has_many(
  :shares,
  class_name: "PostShare",
  foreign_key: :post_id,
  primary_key: :id
  )


end