require 'bcrypt'

class User < ActiveRecord::Base
   attr_accessible :email, :password_digest, :password, :session_token, :reset_token

   validates :email, :password_digest, presence: true
   validates :email, uniqueness: true

   has_many(
   :posts,
   class_name: "Post",
   foreign_key: :user_id,
   primary_key: :id
   )

   has_many(
   :memberships,
   class_name: "FriendCircleMembership",
   foreign_key: :user_id,
   primary_key: :id,
   dependent: :destroy
   )

   has_many :friend_circles, :through => :memberships, source: :friend_circle

   def password=(secret)
      self.password_digest = BCrypt::Password.create(secret)
   end

   def is_password?(secret)
     BCrypt::Password.new(self.password_digest).is_password?(secret)
   end

   def reset_session_token
     self.session_token = self.class.generate_session_token
     self.save!
   end

   def reset_password_token
     self.reset_token = SecureRandom.base64(8)
     self.save!
     self.reset_token
   end

   def self.generate_session_token
     SecureRandom.base64(16)
   end

   def self.find_by_credentials(email, password)
     user = User.find_by_email(email)
     if user.is_password?(password)
       user
     else
       flash[:error] ||= []
       flash[:error] << "Invalid credentials"

       nil
     end
   end

end
