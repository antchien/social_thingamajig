class FriendCircle < ActiveRecord::Base
  attr_accessible :name



  has_many(
  :shared_posts,
  class_name: "PostShare",
  foreign_key: :friend_circle_id,
  primary_key: :id
  )

  has_many(
  :friend_circle_memberships,
  class_name: "FriendCircleMembership",
  foreign_key: :friend_circle_id,
  primary_key: :id
  )


  has_many :members, :through => :friend_circle_memberships, source: :member

end