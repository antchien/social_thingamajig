class UserMailer < ActionMailer::Base
  default from: "social_thingamagig@anthony&sam.com"

  def reset_password(user)
    @user = user
    reset_token = user.reset_password_token
    @url = 'http://localhost:3000/users/reset_token?token=' + reset_token
    mail(to: user.email, subject: "Please reset your password")
  end

end
