class PostsController < ApplicationController

  def index
    user = User.find(params[:user_id])
    @posts = user.posts
    render :index
  end

  def new
    @user = User.find(params[:user_id])
    @friend_circles = @user.friend_circles
    render :new
  end

  def show
    @post = Post.find(params[:id])
    render :show
  end

  def create
    new_post = Post.create(params[:post])
    params[:friend_circle_id].each do |id|
      PostShare.create(friend_circle_id: id, post_id: new_post.id)
    end
    new_post.links.new(params[:links].values)
    redirect_to new_post
  end
      redirect_to new_circle
  end

  def edit
  end

  def update
    user = current_user
    user.update_attributes(params[:user])
    user.save!
    render :index
  end

  def destroy
  end

  def reset_token

    input_reset_token = params[:token]
    @user = User.find_by_reset_token(input_reset_token)
    if @user
      log_in(@user)

      redirect_to edit_user_url(current_user.id)
    else
      flash[:error] ||= []
      flash[:error] << "Invalid token"

      redirect_to new_session_url
    end
  end


end
