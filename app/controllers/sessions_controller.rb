class SessionsController < ApplicationController

  def create
    user = User.find_by_credentials(params[:user][:email], params[:user][:password])
    log_in(user)

    redirect_to user
  end

  def destroy
    user = current_user
    log_out(user)
  end

end
