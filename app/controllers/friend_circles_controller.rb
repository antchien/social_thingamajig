class FriendCirclesController < ApplicationController

  def index
    @friend_circles = FriendCircle.all
    render :index
  end

  def new
    @friend_circles = FriendCircle.all
    @users = User.all
    render :new
  end

  def show
    @current_circle = FriendCircle.find(params[:id])
    @members = @current_circle.members
    render :show
  end

  def create
    new_circle = FriendCircle.create(params[:friend_circle])
    (params[:user][:id]).each do |id|
      FriendCircleMembership.create(user_id: id.to_i, friend_circle_id: new_circle.id.to_i)
    end
      redirect_to new_circle
  end

  def edit
  end

  def update
    user = current_user
    user.update_attributes(params[:user])
    user.save!
    render :index
  end

  def destroy
  end

  def reset_token

    input_reset_token = params[:token]
    @user = User.find_by_reset_token(input_reset_token)
    if @user
      log_in(@user)

      redirect_to edit_user_url(current_user.id)
    else
      flash[:error] ||= []
      flash[:error] << "Invalid token"

      redirect_to new_session_url
    end
  end

end


