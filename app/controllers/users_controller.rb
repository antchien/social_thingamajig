class UsersController < ApplicationController

  def index
    @user = User.all
    render :index
  end

  def new
    render :new
  end

  def show
  end

  def create
    user = User.new(params[:user])
    if user.save
      log_in(user)
      redirect_to users_url
    else
      flash[:error] ||= []
      flash[:error] << user.errors.full_messages
      redirect_to new_users_url
    end
  end

  def edit
  end

  def update
    user = current_user
    user.update_attributes(params[:user])
    user.save!
    render :index
  end

  def destroy
  end

  def reset_token

    input_reset_token = params[:token]
    @user = User.find_by_reset_token(input_reset_token)
    if @user
      log_in(@user)

      redirect_to edit_user_url(current_user.id)
    else
      flash[:error] ||= []
      flash[:error] << "Invalid token"

      redirect_to new_session_url
    end
  end

end
